#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from datetime import date, timedelta
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool


#Taken from Graham Fawcett:
#http://code.activestate.com/recipes/476197/
def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m-1, 12)
    return date(y+a, m+1, 1)

def get_last_day(dt):
    return get_first_day(dt, 0, 1) + timedelta(-1)


class SalesProductPeriod(ModelSQL, ModelView):
    'Sales per Product per Period'
    _name = 'sale.sales_product_period'
    _description = __doc__

    product = fields.Many2One('product.product', 'Product')
    quantity1 = fields.Float('Qty This Month',
            digits=(16, 2))
    amount1 = fields.Float('Amt This Month',
            digits=(16, 2))
    quantity3 = fields.Float('Qty 3 Mth',
            digits=(16, 2))
    amount3 = fields.Float('Amt 3 Mth',
            digits=(16, 2))
    quantity6 = fields.Float('Qty 6 Mth',
            digits=(16, 2))
    amount6 = fields.Float('Amt 6 Mth',
            digits=(16, 2))
    quantity12 = fields.Float('Qty 12 Mth',
            digits=(16, 2))
    amount12 = fields.Float('Amt 12 Mth',
            digits=(16, 2))
    quantity = fields.Float('Total Quantity',
            digits=(16, 2))
    amount = fields.Float('Total Amount',
            digits=(16, 2))

    def table_query(self):
        clause = ' '
        args = []
        today = date.today()
        args.append(today.month)
        args.append(today.year)
        args.append(today.month)
        args.append(today.year)

        for i in [-3,-3,-6,-6,-12,-12]:
            args.append(get_first_day(today, d_months=i))
            args.append(get_last_day(get_first_day(today, d_months=-1)))

        if Transaction().context.get('party'):
            clause += 'AND sale_sale.party = %s '
            args.append(Transaction().context['party'])

        return ('SELECT DISTINCT(sale_line.product) AS id, ' \
                    'MAX(sale_line.create_uid) AS create_uid, ' \
                    'MAX(sale_line.create_date) AS create_date, ' \
                    'MAX(sale_line.write_uid) AS write_uid, ' \
                    'MAX(sale_line.write_date) AS write_date, ' \
                    'product, ' \
                    'SUM(CASE WHEN EXTRACT(MONTH FROM sale_sale.sale_date) = %s AND EXTRACT(YEAR FROM sale_sale.sale_date) = %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity1, ' \
                    'SUM(CASE WHEN EXTRACT(MONTH FROM sale_sale.sale_date) = %s AND EXTRACT(YEAR FROM sale_sale.sale_date) = %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount1, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity3, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount3, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity6, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount6, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity12, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount12, ' \
                    'SUM(COALESCE(sale_line.quantity, 0)) AS quantity, ' \
                    'SUM(COALESCE(sale_line.quantity * sale_line.unit_price, 0)) AS amount ' \
                'FROM sale_line, sale_sale ' \
                'WHERE True ' \
                'AND sale_line.sale = sale_sale.id '\
                "AND sale_sale.state IN ('confirmed','done') "\
                + clause + \
                'GROUP BY product', args)

SalesProductPeriod()


class SalesPartyPeriod(ModelSQL, ModelView):
    'Sales per Party per Period'
    _name = 'sale.sales_party_period'
    _description = __doc__

    party = fields.Many2One('party.party', 'Party')
    quantity1 = fields.Float('Qty This Month',
            digits=(16, 2))
    amount1 = fields.Float('Amt This Month',
            digits=(16, 2))
    quantity3 = fields.Float('Qty 3 Mth',
            digits=(16, 2))
    amount3 = fields.Float('Amt 3 Mth',
            digits=(16, 2))
    quantity6 = fields.Float('Qty 6 Mth',
            digits=(16, 2))
    amount6 = fields.Float('Amt 6 Mth',
            digits=(16, 2))
    quantity12 = fields.Float('Qty 12 Mth',
            digits=(16, 2))
    amount12 = fields.Float('Amt 12 Mth',
            digits=(16, 2))
    quantity = fields.Float('Total Quantity',
            digits=(16, 2))
    amount = fields.Float('Total Amount',
            digits=(16, 2))

    def table_query(self):
        clause = ' '
        args = []
        today = date.today()
        args.append(today.month)
        args.append(today.year)
        args.append(today.month)
        args.append(today.year)

        for i in [-3,-3,-6,-6,-12,-12]:
            args.append(get_first_day(today, d_months=i))
            args.append(get_last_day(get_first_day(today, d_months=-1)))

        if Transaction().context.get('product'):
            clause += 'AND sale_line.product = %s '
            args.append(Transaction().context['product'])

        return ('SELECT DISTINCT(sale_sale.party) AS id, ' \
                    'MAX(sale_line.create_uid) AS create_uid, ' \
                    'MAX(sale_line.create_date) AS create_date, ' \
                    'MAX(sale_line.write_uid) AS write_uid, ' \
                    'MAX(sale_line.write_date) AS write_date, ' \
                    'party, ' \
                    'SUM(CASE WHEN EXTRACT(MONTH FROM sale_sale.sale_date) = %s AND EXTRACT(YEAR FROM sale_sale.sale_date) = %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity1, ' \
                    'SUM(CASE WHEN EXTRACT(MONTH FROM sale_sale.sale_date) = %s AND EXTRACT(YEAR FROM sale_sale.sale_date) = %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount1, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity3, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount3, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity6, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount6, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity12, ' \
                    'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount12, ' \
                    'SUM(COALESCE(sale_line.quantity, 0)) AS quantity, ' \
                    'SUM(COALESCE(sale_line.quantity * sale_line.unit_price, 0)) AS amount ' \
                'FROM sale_line, sale_sale ' \
                'WHERE True ' \
                'AND sale_line.sale = sale_sale.id '\
                "AND sale_sale.state IN ('confirmed','done') "\
                + clause + \
                'GROUP BY party', args)

SalesPartyPeriod()


class SalesProductPeriodInit(ModelView):
    'Sales per Product per Period'
    _name = 'sale.sales_product_period.init'
    _description = __doc__
    party = fields.Many2One('party.party', 'Party')

SalesProductPeriodInit()


class SalesPartyPeriodInit(ModelView):
    'Sales per Party per Period'
    _name = 'sale.sales_party_period.init'
    _description = __doc__
    product = fields.Many2One('product.product', 'Product')

SalesPartyPeriodInit()


class OpenSalesProductPeriod(Wizard):
    'Open Sales per Product per Period'
    _name = 'sale.open_sales_product_period'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'sale.sales_product_period.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open',
                'state': 'end',
            },
        },
    }

    def _action_open(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_sales_product_period_view_form'),
            ('module', '=', 'sale_reporting'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
            'party': data['form']['party'],
            })
        return res

OpenSalesProductPeriod()


class OpenSalesPartyPeriod(Wizard):
    'Open Sales per Party per Period'
    _name = 'sale.open_sales_party_period'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'sale.sales_party_period.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open',
                'state': 'end',
            },
        },
    }

    def _action_open(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_sales_party_period_view_form'),
            ('module', '=', 'sale_reporting'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
            'product': data['form']['product'],
            })
        return res

OpenSalesPartyPeriod()
