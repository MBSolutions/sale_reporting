#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from datetime import date, timedelta
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool


#Taken from Graham Fawcett:
#http://code.activestate.com/recipes/476197/
def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m-1, 12)
    return date(y+a, m+1, 1)

def get_last_day(dt):
    return get_first_day(dt, 0, 1) + timedelta(-1)


class SalesProductMonth(ModelSQL, ModelView):
    'Sales per Product per Month'
    _name = 'sale.sales_product_month'
    _description = __doc__

    product = fields.Many2One('product.product', 'Product')
    quantity1 = fields.Float('Qty Jan',
            digits=(16, 2))
    amount1 = fields.Float('Amt Jan',
            digits=(16, 2))
    quantity2 = fields.Float('Qty Feb',
            digits=(16, 2))
    amount2 = fields.Float('Amt Feb',
            digits=(16, 2))
    quantity3 = fields.Float('Qty Mar',
            digits=(16, 2))
    amount3 = fields.Float('Amt Mar',
            digits=(16, 2))
    quantity4 = fields.Float('Qty Apr',
            digits=(16, 2))
    amount4 = fields.Float('Amt Apr',
            digits=(16, 2))
    quantity5 = fields.Float('Qty May',
            digits=(16, 2))
    amount5 = fields.Float('Amt May',
            digits=(16, 2))
    quantity6 = fields.Float('Qty Jun',
            digits=(16, 2))
    amount6 = fields.Float('Amt Jun',
            digits=(16, 2))
    quantity7 = fields.Float('Qty Jul',
            digits=(16, 2))
    amount7 = fields.Float('Amt Jul',
            digits=(16, 2))
    quantity8 = fields.Float('Qty Aug',
            digits=(16, 2))
    amount8 = fields.Float('Amt Aug',
            digits=(16, 2))
    quantity9 = fields.Float('Qty Sep',
            digits=(16, 2))
    amount9 = fields.Float('Amt Sep',
            digits=(16, 2))
    quantity10 = fields.Float('Qty Oct',
            digits=(16, 2))
    amount10 = fields.Float('Amt Oct',
            digits=(16, 2))
    quantity11 = fields.Float('Qty Nov',
            digits=(16, 2))
    amount11 = fields.Float('Amt Nov',
            digits=(16, 2))
    quantity12 = fields.Float('Qty Dec',
            digits=(16, 2))
    amount12 = fields.Float('Amt Dec',
            digits=(16, 2))
    quantity = fields.Float('Total Quantity',
            digits=(16, 2))
    amount = fields.Float('Total Amount',
            digits=(16, 2))

    def table_query(self):
        clause = ' '
        xclause = ' '
        args = []
        if Transaction().context.get('year'):
            year = Transaction().context['year']
        else:
            year = date.today().year
        start = date(year, 1, 1)
        for i in range(12):
            for x in range(2):
                args.append(get_first_day(start, d_months=i))
                args.append(get_last_day(get_first_day(start, d_months=i)))
            xclause += 'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity'+str(i+1)+', '
            xclause += 'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount'+str(i+1)+', '

        if Transaction().context.get('party'):
            clause += 'AND sale_sale.party = %s '
            args.append(Transaction().context['party'])

        return ('SELECT DISTINCT(sale_line.product) AS id, ' \
                    'MAX(sale_line.create_uid) AS create_uid, ' \
                    'MAX(sale_line.create_date) AS create_date, ' \
                    'MAX(sale_line.write_uid) AS write_uid, ' \
                    'MAX(sale_line.write_date) AS write_date, ' \
                    'product, ' \
                    + xclause + \
                    'SUM(COALESCE(sale_line.quantity, 0)) AS quantity, ' \
                    'SUM(COALESCE(sale_line.quantity * sale_line.unit_price, 0)) AS amount ' \
                'FROM sale_line, sale_sale ' \
                'WHERE True ' \
                'AND sale_line.sale = sale_sale.id '\
                "AND sale_sale.state IN ('confirmed','done') "\
                + clause + \
                'GROUP BY product', args)

SalesProductMonth()


class SalesPartyMonth(ModelSQL, ModelView):
    'Sales per Party per Month'
    _name = 'sale.sales_party_month'
    _description = __doc__

    party = fields.Many2One('party.party', 'Party')
    quantity1 = fields.Float('Qty Jan',
            digits=(16, 2))
    amount1 = fields.Float('Amt Jan',
            digits=(16, 2))
    quantity2 = fields.Float('Qty Feb',
            digits=(16, 2))
    amount2 = fields.Float('Amt Feb',
            digits=(16, 2))
    quantity3 = fields.Float('Qty Mar',
            digits=(16, 2))
    amount3 = fields.Float('Amt Mar',
            digits=(16, 2))
    quantity4 = fields.Float('Qty Apr',
            digits=(16, 2))
    amount4 = fields.Float('Amt Apr',
            digits=(16, 2))
    quantity5 = fields.Float('Qty May',
            digits=(16, 2))
    amount5 = fields.Float('Amt May',
            digits=(16, 2))
    quantity6 = fields.Float('Qty Jun',
            digits=(16, 2))
    amount6 = fields.Float('Amt Jun',
            digits=(16, 2))
    quantity7 = fields.Float('Qty Jul',
            digits=(16, 2))
    amount7 = fields.Float('Amt Jul',
            digits=(16, 2))
    quantity8 = fields.Float('Qty Aug',
            digits=(16, 2))
    amount8 = fields.Float('Amt Aug',
            digits=(16, 2))
    quantity9 = fields.Float('Qty Sep',
            digits=(16, 2))
    amount9 = fields.Float('Amt Sep',
            digits=(16, 2))
    quantity10 = fields.Float('Qty Oct',
            digits=(16, 2))
    amount10 = fields.Float('Amt Oct',
            digits=(16, 2))
    quantity11 = fields.Float('Qty Nov',
            digits=(16, 2))
    amount11 = fields.Float('Amt Nov',
            digits=(16, 2))
    quantity12 = fields.Float('Qty Dec',
            digits=(16, 2))
    amount12 = fields.Float('Amt Dec',
            digits=(16, 2))
    quantity = fields.Float('Total Quantity',
            digits=(16, 2))
    amount = fields.Float('Total Amount',
            digits=(16, 2))

    def table_query(self):
        clause = ' '
        xclause = ' '
        args = []
        if Transaction().context.get('year'):
            year = Transaction().context['year']
        else:
            year = date.today().year
        start = date(year, 1, 1)
        for i in range(12):
            for x in range(2):
                args.append(get_first_day(start, d_months=i))
                args.append(get_last_day(get_first_day(start, d_months=i)))
            xclause += 'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity, 0) ELSE 0 END) AS quantity'+str(i+1)+', '
            xclause += 'SUM(CASE WHEN sale_sale.sale_date >= %s AND sale_sale.sale_date <= %s THEN COALESCE(sale_line.quantity * sale_line.unit_price, 0) ELSE 0 END) AS amount'+str(i+1)+', '

        if Transaction().context.get('product'):
            clause += 'AND sale_line.product = %s '
            args.append(Transaction().context['product'])

        return ('SELECT DISTINCT(sale_sale.party) AS id, ' \
                    'MAX(sale_line.create_uid) AS create_uid, ' \
                    'MAX(sale_line.create_date) AS create_date, ' \
                    'MAX(sale_line.write_uid) AS write_uid, ' \
                    'MAX(sale_line.write_date) AS write_date, ' \
                    'party, ' \
                    + xclause + \
                    'SUM(COALESCE(sale_line.quantity, 0)) AS quantity, ' \
                    'SUM(COALESCE(sale_line.quantity * sale_line.unit_price, 0)) AS amount ' \
                'FROM sale_line, sale_sale ' \
                'WHERE True ' \
                'AND sale_line.sale = sale_sale.id '\
                "AND sale_sale.state IN ('confirmed','done') "\
                + clause + \
                'GROUP BY party', args)

SalesPartyMonth()


class SalesProductMonthInit(ModelView):
    'Sales per Product per Month'
    _name = 'sale.sales_product_month.init'
    _description = __doc__
    year = fields.Integer('Year')
    party = fields.Many2One('party.party', 'Party')

SalesProductMonthInit()


class SalesPartyMonthInit(ModelView):
    'Sales per Party per Month'
    _name = 'sale.sales_party_month.init'
    _description = __doc__
    year = fields.Integer('Year')
    product = fields.Many2One('product.product', 'Product')

SalesPartyMonthInit()


class OpenSalesProductMonth(Wizard):
    'Open Sales per Product per Month'
    _name = 'sale.open_sales_product_month'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'sale.sales_product_month.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open',
                'state': 'end',
            },
        },
    }

    def _action_open(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_sales_product_month_view_form'),
            ('module', '=', 'sale_reporting'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
                    'year': data['form']['year'],
                    'party': data['form']['party'],
                })
        return res

OpenSalesProductMonth()


class OpenSalesPartyMonth(Wizard):
    'Open Sales per Party per Month'
    _name = 'sale.open_sales_party_month'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'sale.sales_party_month.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open',
                'state': 'end',
            },
        },
    }

    def _action_open(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_sales_party_month_view_form'),
            ('module', '=', 'sale_reporting'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
                    'year': data['form']['year'],
                    'product': data['form']['product'],
                })
        return res

OpenSalesPartyMonth()
